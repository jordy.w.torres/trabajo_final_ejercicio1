/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.utiles;

import com.google.gson.Gson;
import Controlador.tda.lista.ListaEnlazada;
import Controlador.tda.lista.ListaEnlazadaServices;
import Controlador.tda.lista.exception.PosicionException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import Modelo.Admin.Admin;
import org.apache.log4j.Logger;

/**
 *
 * @author Yovin
 */
public class GenerarAdmin {

    ListaEnlazadaServices<Admin> lista = new ListaEnlazadaServices<>();
   
    public void guardar() throws PosicionException, IOException {

        Gson json = new Gson();
        Admin[] arrayP = new Admin[2];
        for (int i = 0; i < 2; i++) {
            arrayP[i] = lista.obtenerDato(i);
        }
        String jsonString = json.toJson(arrayP);
        FileWriter file = new FileWriter("datos" + File.separatorChar + "admin" + File.separatorChar+"Admins.json");
        file.write(jsonString);
        file.flush();
    }

    public String nombre() {
        String banco1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String bancoLetras = "abcdefghijklmnopqrstuvwxyz";
        String bancoVocales = "aeiou";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < (Math.random() * (6 - 4)) + 4; i++) {
            if (i == 0) {
                char caracterAleatorio = banco1.charAt((int) (Math.random() * ((banco1.length() - 1) - 0)) + 0);
                sb.append(caracterAleatorio);
            } else if (i / 2 == (double) i / 2.00) {
                char caracterAleatorio = bancoLetras.charAt((int) (Math.random() * ((bancoLetras.length() - 1) - 0)) + 0);
                sb.append(caracterAleatorio);
            } else {
                char caracterAleatorio = bancoVocales.charAt((int) (Math.random() * ((bancoVocales.length() - 1) - 0)) + 0);
                sb.append(caracterAleatorio);
            }
        }
        return String.valueOf(sb);
    }

    public void agregar() {
        for (int i = 0; i < 2; i++) {

            String nombre = nombre(), apellido = nombre();
          //  String cedula = String.valueOf(1950083533 + (int) (Math.random() * (100900000 - 1)) + 1);
            String password = "fank";
            lista.insertarAlInicio(new Admin(nombre, apellido, password,false));
        }

    }

    public static void main(String[] args) throws PosicionException, IOException {
        // TODO code application logic here
        GenerarAdmin o = new GenerarAdmin();
     o.agregar();

  o.guardar();
  
    }

}
