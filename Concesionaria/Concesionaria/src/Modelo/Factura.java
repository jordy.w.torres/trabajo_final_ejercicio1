/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

/**
 *
 * @author Jordy
 */
public class Factura {
    private Integer nFactura;
    private Cliente cliente;
    private Auto auto;

    public Integer getnFactura() {
        return nFactura;
    }

    public void setnFactura(Integer nFactura) {
        this.nFactura = nFactura;
    }

    public Factura(Integer numero,Cliente cliente, Auto auto) {
       this.nFactura=numero;
        this.cliente = cliente;
        this.auto = auto;
    }
    
    public Cliente getC() {
        return cliente;
    }

    public void setC(Cliente c) {
        this.cliente = c;
    }

    public Auto getA() {
        return auto;
    }

    public void setA(Auto a) {
        this.auto = auto;
    }
    
}
